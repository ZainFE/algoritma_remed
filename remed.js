function anagrams(str1,str2){

    //spliting string into array   
    let arr1 = str1.split("");
    let arr2 = str2.split("");
  
    //verifying array lengths
    if(arr1.length !== arr2.length){
        return false;
    }     
  
    //creating objects
    let frqcounter1={};
    let frqcounter2 ={};
  
  //   looping through array elements and keeping count
    for(let val of arr1){
       frqcounter1[val] =(frqcounter1[val] || 0) + 1; 
    }
    for(let val of arr2){
       frqcounter2[val] =(frqcounter2[val] || 0) + 1; 
    }
  
    // console.log(frqcounter1);
    // console.log(frqcounter2);
  
    //loop for every key in first object
    for(let key in frqcounter1){
        //if second object does not contain same frq count
        if(frqcounter2[key] !== frqcounter1[key]){
          return false;
        }
    }
    return true;
  
  }
  console.log(anagrams('aaz','zza'));
  console.log(anagrams('anagrams','nagramas'));